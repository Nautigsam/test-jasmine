function runScript({ fetch, Odigo }) {
  // Paste the script here
  fetch().then(function (code) {
    Odigo.doSomething(code);
  });
}
