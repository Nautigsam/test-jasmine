describe("Script", function () {
  let Odigo;

  beforeEach(function () {
    Odigo = {
      doSomething() {},
    };
    spyOn(Odigo, "doSomething");
  });

  it("should call Odigo with received code", async function () {
    const fetch = function () {
      return Promise.resolve("OK");
    };
    await runScript({
      fetch,
      Odigo,
    });
    expect(Odigo.doSomething).toHaveBeenCalledWith("OK");
  });
});
